package com.demoangular1x.service;

import com.demoangular1x.entity.User;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Service;

/**
 *
 * @author Alejandrino Ortiz.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final AtomicLong counter = new AtomicLong();
    private static final List<User> users;

    static {
        users = populateDummyUsers();
    }

    /**
     * Obtiene todos los registros.
     *
     * @return
     */
    @Override
    public List<User> findAllUsers() {
        return users;
    }

    /**
     * Obtiene registro filtrado por id.
     *
     * @param id
     * @return
     */
    @Override
    public User findById(long id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    /**
     * Obtiene el registro filtrao por nombre.
     *
     * @param name
     * @return
     */
    @Override
    public User findByName(String name) {
        for (User user : users) {
            if (user.getUsername().equalsIgnoreCase(name)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Agrega nuevo registro.
     *
     * @param user
     */
    @Override
    public void saveUser(User user) {
        user.setId(counter.incrementAndGet());
        users.add(user);
    }

    /**
     * Actualiza el registro.
     *
     * @param user
     */
    @Override
    public void updateUser(User user) {
        int index = users.indexOf(user);
        users.set(index, user);
    }

    /**
     * Elimina un registro.
     *
     * @param id
     */
    @Override
    public void deleteUserById(long id) {

        for (Iterator<User> iterator = users.iterator(); iterator.hasNext();) {
            User user = iterator.next();
            if (user.getId() == id) {
                iterator.remove();
            }
        }
    }

    /**
     * Verifica si existe un registro.
     *
     * @param user
     * @return
     */
    @Override
    public boolean isUserExist(User user) {
        return findByName(user.getUsername()) != null;
    }

    /**
     * Elimina todos los registros.
     */
    @Override
    public void deleteAllUsers() {
        users.clear();
    }

    /**
     * Crea lista inicial de registros. -datps dummy-.
     *
     * @return
     */
    private static List<User> populateDummyUsers() {
        List<User> listUsers;
        listUsers = new ArrayList<>();
        listUsers.add(new User(counter.incrementAndGet(), "Sam", "NY", "sam@abc.com"));
        listUsers.add(new User(counter.incrementAndGet(), "Tomy", "ALBAMA", "tomy@abc.com"));
        listUsers.add(new User(counter.incrementAndGet(), "Kelly", "NEBRASKA", "kelly@abc.com"));
        return listUsers;
    }
}
