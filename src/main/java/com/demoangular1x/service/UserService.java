package com.demoangular1x.service;

import com.demoangular1x.entity.User;
import java.util.List;

/**
 *
 * @author Alejandrino Ortiz.
 */
public interface UserService {

    User findById(long id);

    User findByName(String name);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserById(long id);

    List<User> findAllUsers();

    void deleteAllUsers();

    public boolean isUserExist(User user);
}
