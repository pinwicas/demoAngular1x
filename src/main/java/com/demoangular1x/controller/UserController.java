package com.demoangular1x.controller;

import com.demoangular1x.entity.User;
import com.demoangular1x.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Alejandrino Ortiz.
 */
@Controller
public class UserController {

    @Autowired
    UserService userService;
    ResponseEntity responseEntity;

    /**
     * Obtiene el listado de registros.
     *
     * @return
     */
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity listAllUsers() {
        System.out.println("List users");
        List<User> users = userService.findAllUsers();
        responseEntity = new ResponseEntity(users, HttpStatus.OK);
        if (users.isEmpty()) {
            responseEntity = new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }

        return responseEntity;
    }

    /**
     * Obtiene detalle de un registro filtrado por ID.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getUser(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        responseEntity = new ResponseEntity(user, HttpStatus.OK);
        if (user == null) {
            responseEntity = new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    /**
     * Crea un nuevo Registro.
     *
     * @param user
     * @param ucBuilder
     * @return
     */
    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity createUser(@RequestBody User user, UriComponentsBuilder ucBuilder) {
        System.out.println("init add user");
        if (userService.isUserExist(user)) {
            System.out.println("A User with name " + user.getUsername() + " exist!");
            responseEntity = new ResponseEntity(HttpStatus.CONFLICT);
        } else {
            userService.saveUser(user);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
            responseEntity = new ResponseEntity(headers, HttpStatus.OK);
        }
        return responseEntity;
    }

    /**
     * Actualiza un registro filtrado por ID.
     *
     * @param id
     * @param user
     * @return
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        User currentUser = userService.findById(id);
        if (currentUser == null) {
            System.out.println("User with id " + id + " not found! ");
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            currentUser.setUsername(user.getUsername());
            currentUser.setAddress(user.getAddress());
            currentUser.setEmail(user.getEmail());
            userService.updateUser(currentUser);
            responseEntity = new ResponseEntity(currentUser, HttpStatus.OK);

        }
        return responseEntity;
    }

    /**
     * Elimina un registro por id.
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        if (user == null) {
            System.out.println("User with id " + id + " not found! ");
            responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        } else {
            userService.deleteUserById(id);
            responseEntity = new ResponseEntity(HttpStatus.OK);
        }
        return responseEntity;
    }

    @RequestMapping(value = "/user/", method = RequestMethod.DELETE)
    public ResponseEntity deleteAllUsers() {
        userService.deleteAllUsers();
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
