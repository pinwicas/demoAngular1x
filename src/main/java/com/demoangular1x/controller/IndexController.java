package com.demoangular1x.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Alejandrino Ortiz.
 */
@Controller

public class IndexController {

    @RequestMapping(value = "users", method = RequestMethod.GET)
    public String getIndexPage() {
        System.out.println("index");
        return "UserManagement";
    }
}
