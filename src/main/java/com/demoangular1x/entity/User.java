package com.demoangular1x.entity;

import lombok.Data;

/**
 *
 * @author Alejandrino Ortiz Martinez.
 */
@Data
public class User {

    private Long id;
    private String username;
    private String address;
    private String email;

    public User() {
    }

    public User(Long id, String username, String address, String email) {
        this.id = id;
        this.username = username;
        this.address = address;
        this.email = email;
    }
}
