'use strict';
angular.module('myApp').controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
        var self = this;
        self.user = {id: null, username: '', address: '', email: ''};
        self.users = [];

        self.submit = submit;
        self.edit = edit;
        self.remove = remove;
        self.reset = reset;
        self.showForm = showForm;
        self.closeForm = closeForm;
        //events
        $scope.show = {
            form: false,
            grid: true
        }

        $scope.go = function(ai, index) {
            $scope.colorRowSelected = index;
            $scope.rowSelected = ai.id;
        };



        fetchAllUsers();
        function fetchAllUsers() {
            UserService.fetchAllUsers().then(
                    function(d) {
                        self.users = d;
                    },
                    function(errResponse) {
                        console.error('Error while fetching Users');
                    }
            );
        }

        function createUser(user) {
            UserService.createUser(user).then(fetchAllUsers, function(errResponse) {
                console.error('Error while creating Users');
            });
        }
        function updateUser(user, id) {
            UserService.updateUser(user, id)
                    .then(fetchAllUsers, function(errResponse) {
                        console.error('Error while updating User');
                    });
        }

        function deleteUser(id) {
            UserService.deleteUser(id)
                    .then(fetchAllUsers, function(errResponse) {
                        console.error('Error while deleting User');
                    }
                    );
        }
        function showForm() {
            $scope.show.form = true;
            $scope.show.grid = false;
        }
        function closeForm() {
            $scope.show.form = false;
            $scope.show.grid = true;
            reset();
        }
        function submit() {
            if (self.user.id === null) {
                console.log('Saving New User', self.user);
                createUser(self.user);
                $scope.show.form = false;
                $scope.show.grid = true;
            } else {
                updateUser(self.user, self.user.id);
                $scope.show.form = false;
                $scope.show.grid = true;
                console.log('User updated with id ', self.user.id);
            }
            reset();
        }
        function edit(id) {
            console.log('id to be edited', id);
            for (var i = 0; i < self.users.length; i++) {
                if (self.users[i].id === id) {
                    self.user = angular.copy(self.users[i]);
                    $scope.show.form = true;
                    $scope.show.grid = false;
                    break;
                }
            }
        }

        function remove(id) {
            console.log('id to be deleted', id);
            if (self.user.id === id) {//clean form if the user to be deleted is shown there.
                reset();
            }
            deleteUser(id);
        }
        function reset() {
            self.user = {id: null, username: '', address: '', email: ''};
            $scope.myForm.$setPristine(); //reset Form
        }
    }]);