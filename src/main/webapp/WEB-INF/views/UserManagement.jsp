<%--
    Document   : UserManagement
    Created on : 12-abr-2017, 18:12:08
    Author     : Allware7
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Usuarios</title>
        <style>
            .username.ng-valid {
                background-color: lightgreen;
            }
            .username.ng-dirty.ng-invalid-required {
                background-color: red;
            }
            .username.ng-dirty.ng-invalid-minlength {
                background-color: yellow;
            }

            .email.ng-valid {
                background-color: lightgreen;
            }
            .email.ng-dirty.ng-invalid-required {
                background-color: red;
            }
            .email.ng-dirty.ng-invalid-email {
                background-color: yellow;
            }

        </style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
    </head>
    <body ng-app="myApp" class="ng-cloak">
        <hr>
        <div class="container">
            <div class="generic-container" ng-controller="UserController as ctrl">
                <div class="row">
                    <div class="panel panel-default"  ng-show="show.form">
                        <div class="panel-heading"><span class="lead">Usuarios </span></div>
                        <div class="panel-body">
                            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                                <input type="hidden" ng-model="ctrl.user.id" />
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="uname">Nombre</label>
                                    <div class="col-md-7">
                                        <input type="text" ng-model="ctrl.user.username" id="uname" class="username form-control input-sm" placeholder="Enter your name" required ng-minlength="3"/>
                                        <div class="has-error" ng-show="myForm.$dirty">
                                            <span ng-show="myForm.uname.$error.required">This is a required field</span>
                                            <span ng-show="myForm.uname.$error.minlength">Minimum length required is 3</span>
                                            <span ng-show="myForm.uname.$invalid">This field is invalid </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="address">Direcci�n</label>
                                    <div class="col-md-7">
                                        <input type="text" ng-model="ctrl.user.address" id="address" class="form-control input-sm" placeholder="Enter your Address. [This field is validation free]"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="email">Email</label>
                                    <div class="col-md-7">
                                        <input type="email" ng-model="ctrl.user.email" id="email" class="email form-control input-sm" placeholder="Enter your Email" required/>
                                        <div class="has-error" ng-show="myForm.$dirty">
                                            <span ng-show="myForm.email.$error.required">This is a required field</span>
                                            <span ng-show="myForm.email.$invalid">This field is invalid </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-7 text-right">
                                        <a href="#" type="button" ng-click="ctrl.closeForm()" class="btn btn-warning btn-sm" >Cancelar</a>
                                        <input type="submit"  value="{{!ctrl.user.id ? 'Agregar' : 'Actualizar'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row" ng-show="show.grid">
                    <div class="col-md-6">
                        <h3>Usuarios</h3>
                        {{rowSelected}}
                    </div>
                    <div class="col-md-6 text-right">
                        <a href="#" alt="editar" ng-click="ctrl.showForm()" class="btn btn-primary">Nuevo
                            <span class="glyphicon glyphicon-plus" ></span>
                        </a>
                        <a href="#" alt="editar" ng-click="ctrl.edit(rowSelected)" class="btn btn-primary">Editar
                            <span class="glyphicon glyphicon-check" ></span>
                        </a>
                        <a href="#"  ng-click="ctrl.remove(rowSelected)" class="btn btn-danger">Eliminar
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </div>

                <div class="row" ng-show="show.grid">
                    <hr/>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID.</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Email</th>
                                <!-- <th width="20%"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <tr  style="cursor:pointer" ng-class="{
                                        active : $index == colorRowSelected
                                    }"  ng-repeat="u in ctrl.users" ng-click="go(u, $index)" >
                                <td><span ng-bind="u.id"></span></td>
                                <td><span ng-bind="u.username"></span></td>
                                <td><span ng-bind="u.address"></span></td>
                                <td><span ng-bind="u.email"></span></td>
                                <!-- <td>
                                    <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success custom-width">Edit</button>
                                    <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Remove</button>
                                </td> -->
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="<c:url value='/static/js/base/angular1.6.4.js' />"></script>
        <script src="<c:url value='/static/js/base/ui-bootstrap-tpls-2.5.0.js' />"></script>
        <script src="<c:url value='/static/js/app.js' />"></script>
        <script src="<c:url value='/static/js/service/user_service.js' />"></script>
        <script src="<c:url value='/static/js/controller/user_controller.js' />"></script>
    </body>
</html>